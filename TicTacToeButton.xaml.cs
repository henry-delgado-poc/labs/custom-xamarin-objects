﻿using Xamarin.Forms;

namespace CustomXamarinObjects
{
    public partial class TicTacToeButton : Button
    {
        public TicTacToeButton()
        {
            InitializeComponent();
            this.BackgroundColor = DefaultBackgroundColor = Color.Fuchsia;
            this.TextColor = DefaultTextColor = Color.White;
        }

        public string Player { get; set; } = "";

        public long TopicId { get; set; } = 0;
        public long QuestionId { get; set; } = 0;
        public long CorrectAnswerId { get; set; } = 0;


        public bool IsAvailable { get; set; } = true;
        public int BoardIndex { get; set; } = 0;
        public Color FlippedBackgroundColor { get; set; }
        public Color FlippedTextColor { get; set; }
        public Color DefaultBackgroundColor { get; set; } = Color.Fuchsia;
        public Color DefaultTextColor { get; set; } = Color.White;


        public void Reset()
        {
            Player = this.Text = "";
            IsAvailable = true;
            TopicId = 0;
            QuestionId = 0;
            CorrectAnswerId = 0;
            this.BackgroundColor = DefaultBackgroundColor;
            this.TextColor = DefaultTextColor;
        }
    }
}
